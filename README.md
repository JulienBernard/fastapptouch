# FastAppTouch

FAT (FastAppTouch) Copyright (C) 2013-2014 FasTouch [FasTouch.fr ](http://fastouch.fr)

D�velopp� pour l'entreprise FastTouch, FAT est un framework C# de manipulation d'�v�nements tactiles mise en place pour g�n�rer tr�s simplement diff�rent modules multitouch.

## Gestion des branches
Dans leur ordre d'importance par rapport aux autres :

* [MASTER] contient la documentation finale et la derni�re version "release" stable, maintient le versionning
* [RELEASE] contient les derni�res sources du projet en vue de la sortie d'une nouvelle version (validation, test et debug)
* [DEVELOP] contient les sources du projet (en cours de d�veloppement)
* [FEATURE BRANCH] contient les sources d'une fonctionnalit� avant son impl�mentation dans DEVELOP
* [DOC] contient la documentation du projet (en cours d'�criture, validation)